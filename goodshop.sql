-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2015 at 02:58 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goodshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `date`, `comment`) VALUES
(1, 'Nhân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Hà', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Trân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Nhân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Hà', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Nhân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Bảo', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'Hiếu', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(2, 'Công', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(2, 'Nhân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(2, 'Hà', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(2, 'Trân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(2, 'Nhân', '2015-12-12 14:28:23', 'Sản phẩm này đẹp '),
(1, 'tranlediemchau', '2015-12-23 12:37:20', '@@'),
(1, 'tranlediemchau', '2015-12-23 12:42:50', '@@'),
(8, 'thanh', '2015-12-24 21:18:20', 'aa');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_06_074631_insertCol_tldChau', 1),
('2015_12_06_075158_insertCol_tldChau', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personals`
--

CREATE TABLE `personals` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jop` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personals`
--

INSERT INTO `personals` (`id`, `fullname`, `address`, `phonenumber`, `jop`) VALUES
(1, 'Trần Lệ Diểm Châu', 'Q11. TpHCM', '3427687', 'Sinh Viên'),
(2, 'Nguyễn Thị Hằng', 'Q11. TpHCM', '3427687', 'Nhân viên văn phòng'),
(3, 'Trần Văn A', 'Q11. TpHCM', '3427687', 'Kỹ sư'),
(4, 'Nguyễn Thành ', 'Q11. TpHCM', '3427687', 'Kỹ sư');

-- --------------------------------------------------------

--
-- Table structure for table `producers`
--

CREATE TABLE `producers` (
  `id` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `producers`
--

INSERT INTO `producers` (`id`, `name`, `address`) VALUES
('NSX0001', 'Adidas', 'Số 106/8-10 Bàu Cát, P. 12, Q. Tân Bình, HCM'),
('NSX0002', 'Fashion', 'Văn phòng: 662/4 Sư Vạn Hạnh (nối dài), P. 12, Q. 10, Tp. Hồ Chí Minh (TPHCM)'),
('NSX0003', 'Pitit', '8/12 Núi Thành, Phường 13, Quận Tân Bình, Tp. Hồ Chí Minh (TPHCM)'),
('NSX0004', 'Nike', 'Số 80, Đường Nguyễn Phong Sắc, P. Dịch Vọng, Q. Cầu Giấy, Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8mb4 DEFAULT NULL,
  `price1` decimal(10,3) DEFAULT NULL,
  `images_url` varchar(45) CHARACTER SET utf8mb4 DEFAULT NULL,
  `price2` decimal(10,3) DEFAULT NULL,
  `discription` text COLLATE utf8_unicode_ci,
  `thumb1` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumb2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `thumb3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `producer` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countaccess` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price1`, `images_url`, `price2`, `discription`, `thumb1`, `thumb2`, `thumb3`, `type`, `producer`, `countaccess`) VALUES
(1, 'Áo Thun Nữ Zara thời trang', '148.000', 'pic2.jpg', '150.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic21.jpg', 'pic22.jpg', 'pic23.jpg', 'LH0001', NULL, 149),
(2, 'Túi xách hợp sang trọng', '300.000', 'pic4.jpg', '270.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic41.jpg', 'pic42.jpg', 'pic43.jpg', 'LH0002', NULL, 23),
(3, 'Áo Thun Nữ Zara thời trang', '277.000', 'pic1.jpg', '280.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic51.jpg', 'pic52.jpg', 'pic53.jpg', 'LH0001', NULL, 28),
(4, 'Áo thun nữ tay dài kẻ sọc', '185.000', 'pic6.jpg', '190.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic61.jpg', 'pic62.jpg', 'pic63.jpg', 'LH0001', NULL, 3),
(6, 'Giày nhung cao gót sang trọng', '277.000', 'pic5.jpg', '280.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic51.jpg', 'pic52.jpg', 'pic53.jpg', 'LH0001', NULL, 2),
(7, 'Áo thun nữ tay dài kẻ sọc thời trang', '185.000', 'pic6.jpg', '190.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic61.jpg', 'pic62.jpg', 'pic63.jpg', 'LH0001', NULL, 1),
(8, 'Áo thun nữ tay dài kẻ sọc thời trang', '185.000', 'pic6.jpg', '190.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic61.jpg', 'pic62.jpg', 'pic63.jpg', 'LH0001', NULL, 3),
(9, 'Áo Thun Nữ Zara thời trang', '148.000', 'pic2.jpg', '150.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic21.jpg', 'pic22.jpg', 'pic23.jpg', 'LH0001', NULL, 14),
(10, 'Túi xách hợp sang trọng', '260.000', 'pic4.jpg', '270.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic41.jpg', 'pic42.jpg', 'pic43.jpg', 'LH0002', NULL, 5),
(11, 'Áo Thun Nữ Zara thời trang', '277.000', 'pic2.jpg', '280.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic51.jpg', 'pic52.jpg', 'pic53.jpg', 'LH0001', NULL, 1),
(12, 'Áo Thun Nữ Zara thời trang', '148.000', 'pic2.jpg', '150.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic21.jpg', 'pic22.jpg', 'pic23.jpg', 'LH0001', NULL, 1),
(13, 'Áo Thun Nữ Zara thời trang', '148.000', 'pic2.jpg', '150.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic21.jpg', 'pic22.jpg', 'pic23.jpg', 'LH0001', NULL, 1),
(14, 'Túi xách hợp sang trọng', '260.000', 'pic4.jpg', '270.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic41.jpg', 'pic42.jpg', 'pic43.jpg', 'LH0002', NULL, 1),
(15, 'Áo Thun Nữ Zara thời trang', '277.000', 'pic1.jpg', '280.000', 'Thiết kế trẻ trung cùng chất liệu sang trọng mang đến phong cách sang trọng, quyến rũ cho bạn gái.Kiểu dáng thời trang giúp bạn thể hiện gu thời trang đẳng cấp, sang trọng cho bạn gái.Thiết kế cổ trụ, nút bấm tinh tế, Form dài tôn dáng, họa tiết thổ cẩm nổi bật, ấn tượng.Áo dày dặn, đường chỉ may chắc chắn, tinh tế cho bạn gái phong cách hoàn hảo.Chất liệu: Dạ bố dày dặn, bên trong lót lớp dù tạo cảm giác mát mẻ và thoải mái cho người mặc.Size: M, L.Hàng nhập.', 'pic51.jpg', 'pic52.jpg', 'pic53.jpg', 'LH0001', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Nhân Viên'),
(3, 'Khách Hàng');

-- --------------------------------------------------------

--
-- Table structure for table `typemarchandises`
--

CREATE TABLE `typemarchandises` (
  `id` varchar(45) CHARACTER SET utf8 NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typemarchandises`
--

INSERT INTO `typemarchandises` (`id`, `name`) VALUES
('LH0001', 'Áo'),
('LH0002', 'Giày'),
('LH0003', 'Quần');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'tranlediemchau', 'tranlediemchau@gmail.com', '$2y$10$.riyjEEEKnZVL7bIUrz1P.JIkEasmwF1uxt09RU7E8.3RmWqB5WHK', 'Ow0xYdAli88lCWK0e1DeRnoVJIPEkfEhYPROkJunAZyNnlimCbNRU2z6uuWS', '2015-12-12 14:28:23', '2015-12-24 16:20:01', 1),
(2, 'hang', 'hang@gmail.com', '$2y$10$R/wG8lIn1F.SgCSEHGhsCuZB5kTgP/sh8JsDqhhxs1.CKYs3Mjy6q', 'Z3jGMsZiBJoHOCnN4hptmFeKfgNgJ94w8ENxCDrslr1PnJassknDzCl65J7E', '2015-12-12 14:53:15', '2015-12-12 15:07:57', 2),
(3, 'abc', 'abc@gmail.com', '$2y$10$w9Z1qrwiIsuEamdMCL9KVuBsVEONdPBgyOINAO1S3UE0kqAmrsEgC', 'BTqrazflCuETy8AhXGD8YW8DlaMYb3JsfeKFeANAKM4zxNGOZxEwtpW9CBSI', '2015-12-12 15:22:40', '2015-12-12 15:22:53', 2),
(4, 'thanh', 'thanh@gmail.com', '$2y$10$MbpnZu.mCFpg4fH557lA0eKRytFlTl2i64esq9RRa3MGFyTeases2', 'pzx4o1KtkniGGPO3zuaGnlsk3FLGila0JZswgAhB67pgcWOqoOIfVbCNXbIO', '2015-12-12 15:23:11', '2015-12-24 16:33:20', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD KEY `s_idx` (`name`),
  ADD KEY `d_idx` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `personals`
--
ALTER TABLE `personals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producers`
--
ALTER TABLE `producers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_a_b_idx` (`type`),
  ADD KEY `pk_product_producer_idx` (`producer`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typemarchandises`
--
ALTER TABLE `typemarchandises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `pk_a_idx` (`role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personals`
--
ALTER TABLE `personals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `d` FOREIGN KEY (`id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `personals`
--
ALTER TABLE `personals`
  ADD CONSTRAINT `j` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `pk_p_t` FOREIGN KEY (`type`) REFERENCES `typemarchandises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pk_product_producer` FOREIGN KEY (`producer`) REFERENCES `producers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `pk_a` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
